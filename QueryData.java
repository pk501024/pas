import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Vector;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.Enumeration;
import java.util.Stack;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import com.google.gson.*;



public class QueryData {
	// Constructor
	QueryData(){

	}
	
	private Stack<String> CalculateStack(HashSet<String> objSet, Stack<String> stack) {
		if(objSet==null) {
    		stack.removeAll(stack);
    		System.out.println("Empty Stack");
    		return stack;
    	}
    	
    	if(stack.isEmpty()) {
    		for(String objUID : objSet){
    			stack.add(objUID);
        	}
    	}else {
    		Stack<String> tstack = new Stack<String>();
    		for(String objUID : objSet){
           		if(stack.search(objUID)!=-1) {
           			tstack.add(objUID);
        		}
    		}
    		if(tstack.isEmpty()){
    			stack.removeAll(stack);
   
    		}
   			stack=tstack;

    	}
		return stack;
	}
	
	private String CheckForUID(String inStr) {
		// Hashtable based text replace function 
		String token=null;
		String patternStr = "/users/(\\d+)$";
        Pattern regex = Pattern.compile(patternStr);
        Matcher m = regex.matcher(inStr);
        if(m.find()){
            token = m.group(1);
        }
        return token;
	}
	
	private String CheckForGID(String inStr) {
		// Hashtable based text replace function 
		String token=null;
		String patternStr = "/groups/(\\d+)$";
        Pattern regex = Pattern.compile(patternStr);
        Matcher m = regex.matcher(inStr);
        if(m.find()){
            token = m.group(1);
        }
        return token;
	}
	
	private String CheckForUIDGroupQuery(String inStr) {
		// Hashtable based text replace function 
		String token=null;
		String patternStr = "/users/(\\d+)/groups";
        Pattern regex = Pattern.compile(patternStr);
        Matcher m = regex.matcher(inStr);
        if(m.find()){
            token = m.group(1);
        }
        return token;
	}

	
	public JsonArray MakeListIntoJsonArray(String str) {
		JsonArray jsonarray = new JsonArray();
		String [] strArr = str.split(","); 
		for(String value: strArr) {
			if(!value.equals("")) {
				jsonarray.add(value);
			}
			
		}
		return jsonarray;
	}
	
	public void QueryIndexedData(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		String contextPath = request.getServletPath(); 
		JsonArray jsonarray = new JsonArray();
		String UID_str=CheckForUID(contextPath);
		String GID_str=CheckForGID(contextPath);
		String UID_str_grp_query=CheckForUIDGroupQuery(contextPath);
		
		
		if( ((UID_str)!=null)  &&  (IndexedDataStore.uid_hashstore.containsKey(UID_str)) ) {
			JsonObject result = new JsonObject();
			PassDataStore data = IndexedDataStore.uid_hashstore.get(UID_str);
			result.addProperty("name", data.Username);
			result.addProperty("uid", data.UID);
			result.addProperty("gid", data.GID);
			result.addProperty("comment", data.Comment);
			result.addProperty("home", data.HomeDir);
			result.addProperty("shell", data.Command);	
			response.getWriter().append(result.toString());
		}else if( ((GID_str)!=null)  &&  (IndexedDataStore.gid_hashstore.containsKey(GID_str)) ) {
			JsonObject result = new JsonObject();
			GroupDataStore data = IndexedDataStore.gid_hashstore.get(GID_str);
			result.addProperty("name", data.Groupname);
			result.addProperty("gid", data.GID);
			result.add("members", new Gson().toJsonTree(MakeListIntoJsonArray(data.GroupList)) );
			response.getWriter().append(result.toString());
		}else if( ((UID_str_grp_query)!=null)  &&  (IndexedDataStore.uid_hashstore.containsKey(UID_str_grp_query)) ) {
	
			Stack<String> stack = new Stack<String>();
			PassDataStore pdata = IndexedDataStore.uid_hashstore.get(UID_str_grp_query);
			GroupDataStore gdata = IndexedDataStore.gid_hashstore.get(pdata.GID);

			HashSet<String> objSet;
        	objSet = (IndexedDataStore.ghashstore.get("members")).get(gdata.Groupname);
        	stack = CalculateStack(objSet, stack);

			// Results of Query
			while(!stack.isEmpty()) {
				JsonObject result = new JsonObject();
				GroupDataStore data = IndexedDataStore.gid_hashstore.get(stack.pop());
				result.addProperty("name", data.Groupname);
				result.addProperty("gid", data.GID);
				result.add("members", new Gson().toJsonTree(MakeListIntoJsonArray(data.GroupList)) );
				jsonarray.add(result);
			}
			response.getWriter().append(jsonarray.toString());
	
		}else if(contextPath.equals("/users")) {
			for(String Key: IndexedDataStore.uid_hashstore.keySet()) {
				JsonObject result = new JsonObject();
				PassDataStore data = IndexedDataStore.uid_hashstore.get(Key);
				result.addProperty("name", data.Username);
				result.addProperty("uid", data.UID);
				result.addProperty("gid", data.GID);
				result.addProperty("comment", data.Comment);
				result.addProperty("home", data.HomeDir);
				result.addProperty("shell", data.Command);	
				jsonarray.add(result);
			}
			response.getWriter().append(jsonarray.toString());
		}else if(contextPath.equals("/groups")) {
			for(String Key: IndexedDataStore.gid_hashstore.keySet()) {
				JsonObject result = new JsonObject();
				GroupDataStore data = IndexedDataStore.gid_hashstore.get(Key);
				result.addProperty("name", data.Groupname);
				result.addProperty("gid", data.GID);
				result.add("members", new Gson().toJsonTree(MakeListIntoJsonArray(data.GroupList)) );
				jsonarray.add(result);
			}
			response.getWriter().append(jsonarray.toString());
		}else if(contextPath.equals("/users/query")) {
			Stack<String> stack = new Stack<String>();
				Enumeration<String> parameterNames = request.getParameterNames();
		        while (parameterNames.hasMoreElements()) {
		            String paramName = parameterNames.nextElement();
		            if(paramName.equals("uid")) {
		            	String UID = request.getParameter("uid");
		            	if((IndexedDataStore.uid_hashstore).get(UID)==null){
		            		stack.removeAll(stack);
		            		System.out.println("Empty Stack");
	            			break;
		            	}
		            	if(stack.isEmpty()) {
		            		stack.add(UID);
		            	}else {
		               		if(stack.search(UID)!=-1) {
		               			Stack<String> tstack = new Stack<String>();
		               			tstack.add(UID);
		               			stack=tstack;
		            		}else {
		            			stack.removeAll(stack);
		            			break;
		            		}
		             
		            	}
		            }else if(paramName.equals("name")) {
		            	HashSet<String> objSet;
		            	String Username = request.getParameter("name");
		            	objSet = (IndexedDataStore.hashstore.get("name")).get(Username);
		            	stack = CalculateStack(objSet, stack);
		            	if(stack.isEmpty()){
	            			break;
	            		}
					}else if(paramName.equals("home")) {
		            	HashSet<String> objSet;
		            	String HomeDir = request.getParameter("home");
		            	objSet = (IndexedDataStore.hashstore.get("home")).get(HomeDir);
		            	stack = CalculateStack(objSet, stack);
		            	if(stack.isEmpty()){
	            			break;
	            		}
					}else if(paramName.equals("comment")) {
		            	HashSet<String> objSet;
		            	String Comment = request.getParameter("comment");
		            	objSet = (IndexedDataStore.hashstore.get("comment")).get(Comment);
		            	stack = CalculateStack(objSet, stack);
		            	if(stack.isEmpty()){
	            			break;
	            		}
					}else if(paramName.equals("shell")) {
		            	HashSet<String> objSet;
		            	String Command = request.getParameter("shell");
		            	objSet = (IndexedDataStore.hashstore.get("shell")).get(Command);
		            	stack = CalculateStack(objSet, stack);
		            	if(stack.isEmpty()){
	            			break;
	            		}
					}else if(paramName.equals("gid")) {
		            	HashSet<String> objSet;
		            	String GID = request.getParameter("gid");          	
		            	objSet = (IndexedDataStore.hashstore.get("gid")).get(GID);
		            	stack = CalculateStack(objSet, stack);
		            	if(stack.isEmpty()){
	            			break;
	            		}
					}
			}
	        if(stack.isEmpty()){
	        	response.setStatus(404);
	        	return;
    		}
			// Results of Queuery
			while(!stack.isEmpty()) {
				JsonObject result = new JsonObject();
				PassDataStore data = IndexedDataStore.uid_hashstore.get(stack.pop());
				result.addProperty("name", data.Username);
				result.addProperty("uid", data.UID);
				result.addProperty("gid", data.GID);
				result.addProperty("comment", data.Comment);
				result.addProperty("home", data.HomeDir);
				result.addProperty("shell", data.Command);	
				jsonarray.add(result);
			}
			response.getWriter().append(jsonarray.toString());
		}else if(contextPath.equals("/groups/query")) {
			boolean mflag=false;
			Stack<String> stack = new Stack<String>();
			Enumeration<String> parameterNames = request.getParameterNames();
	        while (parameterNames.hasMoreElements()) {
	            String paramName = parameterNames.nextElement();
	            if(paramName.equals("gid")) {
	            	String GID = request.getParameter("gid");
	            	if((IndexedDataStore.gid_hashstore).get(GID)==null){
	            		stack.removeAll(stack);
	            		System.out.println("Empty Stack");
            			break;
	            	}
	            	if(stack.isEmpty()) {
	            		stack.add(GID);
	            	}else {
	               		if(stack.search(GID)!=-1) {
	               			Stack<String> tstack = new Stack<String>();
	               			tstack.add(GID);
	               			stack=tstack;
	            		}else {
	            			stack.removeAll(stack);
	            			break;
	            		}
	             
	            	}
	            }else if(paramName.equals("name")) {
	            	HashSet<String> objSet;
	            	String Groupname = request.getParameter("name");
	            	objSet = (IndexedDataStore.ghashstore.get("name")).get(Groupname);
	            	stack = CalculateStack(objSet, stack);
	            	if(stack.isEmpty()){
            			break;
            		}
				}else if(paramName.equals("member")) {
					if(!mflag) {
						// Get all member instances in query and then set mflag to done(true).
						String[] members = request.getParameterValues("member");
						for(String m:members) {
			            	HashSet<String> objSet;
			            	String Member = m;
			            	objSet = (IndexedDataStore.ghashstore.get("members")).get(Member);
			            	stack = CalculateStack(objSet, stack);
						}
						mflag=true;
		            	if(stack.isEmpty()){
	            			break;
	            		}
					}
				}
			}
	        if(stack.isEmpty()){
	        	response.setStatus(404);
	        	return;
    		}
			// Results of Query
			while(!stack.isEmpty()) {
				JsonObject result = new JsonObject();
				GroupDataStore data = IndexedDataStore.gid_hashstore.get(stack.pop());
				result.addProperty("name", data.Groupname);
				result.addProperty("gid", data.GID);
				result.add("members", new Gson().toJsonTree(MakeListIntoJsonArray(data.GroupList)) );
				jsonarray.add(result);
			}
			response.getWriter().append(jsonarray.toString());
		}else {
			response.setStatus(404);
		}
	}
	
}

