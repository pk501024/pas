import java.util.HashMap;
import java.util.HashSet;
import java.util.Vector;

import com.google.gson.JsonArray;



public class IndexedDataStore {
	////////////////// Fully indexed Memory Storage of Password and Group Data  /////////////////////
	// Password hashstores
	static HashMap<String, HashMap<String, HashSet<String>>> hashstore = new HashMap<String, HashMap<String, HashSet<String>>>();
	static HashMap<String, PassDataStore> uid_hashstore = new HashMap<String, PassDataStore>();
	// Group hashstores
	static HashMap<String, HashMap<String, HashSet<String>>> ghashstore = new HashMap<String, HashMap<String, HashSet<String>>>();
	static HashMap<String, GroupDataStore> gid_hashstore = new HashMap<String, GroupDataStore>();

	// Constructor
	IndexedDataStore(){
		
		// Password File Query Parameters
		hashstore.put((String)"name", new HashMap<String, HashSet<String>>());
		hashstore.put((String)"gid", new HashMap<String, HashSet<String>>());
		hashstore.put((String)"comment", new HashMap<String, HashSet<String>>());
		hashstore.put((String)"home", new HashMap<String, HashSet<String>>());
		hashstore.put((String)"shell", new HashMap<String, HashSet<String>>());
		
		// Group File Query Parameters 
		ghashstore.put((String)"name", new HashMap<String, HashSet<String>>());
		ghashstore.put((String)"members", new HashMap<String, HashSet<String>>());
	}
	
	public void AddDataStoreToIndex(PassDataStore datastore){
		// Enter data into a fully indexed Store for Password File Data
		
		// Add datastore object to UID Hashstore
		uid_hashstore.put(datastore.UID, datastore);
		
		// Create the entries in general hashstore for corresponding UID
		HashSet<String> objSet;
		// name
		if(datastore.Username!=null){
			if((hashstore.get("name")).get(datastore.Username)==null) {
				objSet = new HashSet<String>();
			}else {
				objSet = (hashstore.get("name")).get(datastore.Username);
			}
			objSet.add(datastore.UID);
			(hashstore.get("name")).put(datastore.Username, objSet);
		}
		// gid
		if(datastore.GID!=null){
			if((hashstore.get("gid")).get(datastore.GID)==null) {
				objSet = new HashSet<String>();
			}else {
				objSet = (hashstore.get("gid")).get(datastore.GID);
			}
			objSet.add(datastore.UID);
			(hashstore.get("gid")).put(datastore.GID, objSet);			
		}
		
		// comment
		if(datastore.Comment!=null){
			if((hashstore.get("comment")).get(datastore.Comment)==null) {
				objSet = new HashSet<String>();
			}else {
				objSet = (hashstore.get("comment")).get(datastore.Comment);
			}
			objSet.add(datastore.UID);
			(hashstore.get("comment")).put(datastore.Comment, objSet);
		}
		// home
		if(datastore.HomeDir!=null){
			if((hashstore.get("home")).get(datastore.HomeDir)==null) {
				objSet = new HashSet<String>();
			}else {
				objSet = (hashstore.get("home")).get(datastore.HomeDir);
			}
			objSet.add(datastore.UID);
			(hashstore.get("home")).put(datastore.HomeDir, objSet);
		}
		// shell
		if(datastore.Command!=null){
			if((hashstore.get("shell")).get(datastore.Command)==null) {
				objSet = new HashSet<String>();
			}else {
				objSet = (hashstore.get("shell")).get(datastore.Command);
			}
			objSet.add(datastore.UID);
			(hashstore.get("shell")).put(datastore.Command, objSet);
		}
	}
	
	public void AddDataStoreToIndex(GroupDataStore datastore){
		// Enter data into a fully indexed Store for Group File Data
		
		// Add datastore object to UID Hashstore
		gid_hashstore.put(datastore.GID, datastore);
		
		// Create the entries in general hashstore for corresponding UID
		HashSet<String> objSet = new HashSet<String>();
		// name
		if(datastore.GroupList!=null){
			String [] strArr = datastore.GroupList.split(","); 
			for(String value: strArr) {
				if(!value.equals("")) {
					if((ghashstore.get("members")).get(value)==null) {
						objSet = new HashSet<String>();
					}else {
						objSet = (ghashstore.get("members")).get(value);
					}
					objSet.add(datastore.GID);
					(ghashstore.get("members")).put(value, objSet);
				}
			}
		}
		if(datastore.Groupname!=null){
			if((ghashstore.get("name")).get(datastore.Groupname)==null) {
				objSet = new HashSet<String>();
			}else {
				objSet = (ghashstore.get("name")).get(datastore.Groupname);
			}
			objSet.add(datastore.GID);
			(ghashstore.get("name")).put(datastore.Groupname, objSet);
		}
		
	}
	

}
