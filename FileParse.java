import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;

public class FileParse {
	
	public boolean CheckForPassFileUpdate() {	
		String pfilepath = PaS.configprop.getProperty("passwdPath");
		File pfile= new File(pfilepath);
		long passfiletimestamp=pfile.lastModified();
		if(passfiletimestamp>PaS.passfiletimestamp) {
			return true;
		}
		return false;
	}
	
	public boolean CheckForGroupFileUpdate() {	
		String gfilepath = PaS.configprop.getProperty("groupPath");
		File gfile= new File(gfilepath);
		long groupfiletimestamp=gfile.lastModified();
		if(groupfiletimestamp>PaS.groupfiletimestamp) {
			return true;
		}
		return false;
	}
	
	public static synchronized void reloadPassFile() {
		String pfilepath = PaS.configprop.getProperty("passwdPath");
		File pfile= new File(pfilepath);
		long passfiletimestamp=pfile.lastModified();
		if(passfiletimestamp>PaS.passfiletimestamp) {
			FileParse fio = new FileParse();
			fio.parsePasswordFile(pfilepath);
			PaS.passfiletimestamp=passfiletimestamp;
		}
	}
	
	public static synchronized void reloadGroupFile() {
		String gfilepath = PaS.configprop.getProperty("groupPath");
		File gfile= new File(gfilepath);
		long groupfiletimestamp=gfile.lastModified();
		if(groupfiletimestamp>PaS.groupfiletimestamp) {
			FileParse fio = new FileParse();
			fio.parseGroupFile(gfilepath);
			PaS.groupfiletimestamp=groupfiletimestamp;
		}
	}
	

	public boolean parsePasswordFile(String filepath) {	
	  String str;
	  BufferedReader inRead;
	  
	  File pfile= new File(filepath);
	  PaS.passfiletimestamp=pfile.lastModified();
      // Read File
      try{
    	   inRead = new BufferedReader(new FileReader(pfile));
      }catch(FileNotFoundException e){
           System.out.println("File Not Found:" + pfile);
           return false;
      }
      try{
       	while((str=inRead.readLine()) != null){   
       		String [] strArr = str.split(":"); 
       		if(strArr.length==6) {
       			// Handles special case in the split approach where no element is in the last field of the password file
       			strArr = addEmptyString(strArr);
       		}
       		PassDataStore passDataStore= new PassDataStore(strArr);
       		PaS.DataStore.AddDataStoreToIndex(passDataStore);
       	}
       	inRead.close();
      }catch(IOException e){
    	   System.out.println("IOException in Reading File");
     	   return false;
      }
        	
  	return true;
	}
	
	public String[] addEmptyString(String[] strArr) {
		ArrayList<String> al = new ArrayList<String>(Arrays.asList(strArr));
 	    al.add("");
 	    strArr = al.toArray(strArr);
 	    return strArr;
	}
	
	public boolean parseGroupFile(String filepath) {	
		  String str;
		  BufferedReader inRead;
		  
		  File pfile= new File(filepath);
		  PaS.groupfiletimestamp=pfile.lastModified();
	      // Read File
	      try{
	    	   inRead = new BufferedReader(new FileReader(pfile));
	      }catch(FileNotFoundException e){
	           System.out.println("File Not Found:" + pfile);
	           return false;
	      }
	      try{
	    	
	       	while((str=inRead.readLine()) != null){   
	       		String [] strArr = str.split(":"); 
	       		if(strArr.length==3) {
	       			// Handles special case in the split approach where no element is in the last field of the group file
	       			strArr = addEmptyString(strArr);
	       		}
	       		GroupDataStore groupDataStore= new GroupDataStore(strArr);
	       		PaS.DataStore.AddDataStoreToIndex(groupDataStore);
	       	}
	       	inRead.close();
	      }catch(IOException e){
	    	   System.out.println("IOException in Reading File");
	     	   return false;
	      }
	        	
	  	return true;
		}
}
