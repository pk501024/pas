public class GroupDataStore {
	
	GroupDataStore(String[] strarr) throws RuntimeException{
		if((strarr!=null) && strarr.length==4) {
			Groupname=strarr[0];
			Password=strarr[1];
			GID=strarr[2];
			GroupList=strarr[3];
		}else {
			throw new RuntimeException("GroupDataStore: Malformed Input");
		}
	}
	
	String Groupname;
	String Password;
	String GID;
	String GroupList;
}


