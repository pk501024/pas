import static org.junit.Assert.*;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;
 
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
 
import org.junit.Before;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import java.util.Collections;
import java.util.Arrays;





public class JUnitTests {

	public void test(){
        System.out.println("Testing");
        
	}
	
	@Test
	public void testUsersRequest() throws IOException, ServletException {
				
        when(request.getServletPath()).thenReturn("/users");
        when(config.getInitParameter("groupPath")).thenReturn("c:\\\\temp\\group");
        when(config.getInitParameter("passwdPath")).thenReturn("c:\\\\temp\\passwd");
     
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
         
        when(response.getWriter()).thenReturn(pw);
 
        PaS testServlet =new PaS();
        testServlet.init(config);
        testServlet.doGet(request, response);
        String result = sw.getBuffer().toString().trim();
        // System.out.println(result);
        String referenceString = new String("[{\"name\":\"pi\",\"uid\":\"1000\",\"gid\":\"1000\",\"comment\":\",,,\",\"home\":\"/home/pi\",\"shell\":\"/bin/bash\"},{\"name\":\"epmd\",\"uid\":\"110\",\"gid\":\"114\",\"comment\":\"\",\"home\":\"/var/run/epmd\",\"shell\":\"/bin/false\"},{\"name\":\"uucp\",\"uid\":\"10\",\"gid\":\"10\",\"comment\":\"uucp\",\"home\":\"/var/spool/uucp\",\"shell\":\"/usr/sbin/nologin\"},{\"name\":\"www-data\",\"uid\":\"33\",\"gid\":\"33\",\"comment\":\"www-data\",\"home\":\"/var/www\",\"shell\":\"/usr/sbin/nologin\"},{\"name\":\"backup\",\"uid\":\"34\",\"gid\":\"34\",\"comment\":\"backup\",\"home\":\"/var/backups\",\"shell\":\"/usr/sbin/nologin\"},{\"name\":\"proxy\",\"uid\":\"13\",\"gid\":\"13\",\"comment\":\"proxy\",\"home\":\"/bin\",\"shell\":\"/usr/sbin/nologin\"},{\"name\":\"list\",\"uid\":\"38\",\"gid\":\"38\",\"comment\":\"Mailing List Manager\",\"home\":\"/var/list\",\"shell\":\"/usr/sbin/nologin\"},{\"name\":\"irc\",\"uid\":\"39\",\"gid\":\"39\",\"comment\":\"ircd\",\"home\":\"/var/run/ircd\",\"shell\":\"/usr/sbin/nologin\"},{\"name\":\"nobody\",\"uid\":\"65534\",\"gid\":\"65534\",\"comment\":\"nobody\",\"home\":\"/nonexistent\",\"shell\":\"/usr/sbin/nologin\"},{\"name\":\"root\",\"uid\":\"0\",\"gid\":\"0\",\"comment\":\"root\",\"home\":\"/root\",\"shell\":\"/bin/bash\"},{\"name\":\"daemon\",\"uid\":\"1\",\"gid\":\"1\",\"comment\":\"daemon\",\"home\":\"/usr/sbin\",\"shell\":\"/usr/sbin/nologin\"},{\"name\":\"systemd-timesync\",\"uid\":\"100\",\"gid\":\"103\",\"comment\":\"systemd Time Synchronization,,,\",\"home\":\"/run/systemd\",\"shell\":\"/bin/false\"},{\"name\":\"bin\",\"uid\":\"2\",\"gid\":\"2\",\"comment\":\"bin\",\"home\":\"/bin\",\"shell\":\"/usr/sbin/nologin\"},{\"name\":\"systemd-network\",\"uid\":\"101\",\"gid\":\"104\",\"comment\":\"systemd Network Management,,,\",\"home\":\"/run/systemd/netif\",\"shell\":\"/bin/false\"},{\"name\":\"sys\",\"uid\":\"3\",\"gid\":\"3\",\"comment\":\"sys\",\"home\":\"/dev\",\"shell\":\"/usr/sbin/nologin\"},{\"name\":\"systemd-resolve\",\"uid\":\"102\",\"gid\":\"105\",\"comment\":\"systemd Resolver,,,\",\"home\":\"/run/systemd/resolve\",\"shell\":\"/bin/false\"},{\"name\":\"sync\",\"uid\":\"4\",\"gid\":\"65534\",\"comment\":\"sync\",\"home\":\"/bin\",\"shell\":\"/bin/sync\"},{\"name\":\"systemd-bus-proxy\",\"uid\":\"103\",\"gid\":\"106\",\"comment\":\"systemd Bus Proxy,,,\",\"home\":\"/run/systemd\",\"shell\":\"/bin/false\"},{\"name\":\"games\",\"uid\":\"5\",\"gid\":\"60\",\"comment\":\"games\",\"home\":\"/usr/games\",\"shell\":\"/usr/sbin/nologin\"},{\"name\":\"_apt\",\"uid\":\"104\",\"gid\":\"65534\",\"comment\":\"\",\"home\":\"/nonexistent\",\"shell\":\"/bin/false\"},{\"name\":\"man\",\"uid\":\"6\",\"gid\":\"12\",\"comment\":\"man\",\"home\":\"/var/cache/man\",\"shell\":\"/usr/sbin/nologin\"},{\"name\":\"messagebus\",\"uid\":\"105\",\"gid\":\"109\",\"comment\":\"\",\"home\":\"/var/run/dbus\",\"shell\":\"/bin/false\"},{\"name\":\"lp\",\"uid\":\"7\",\"gid\":\"7\",\"comment\":\"lp\",\"home\":\"/var/spool/lpd\",\"shell\":\"/usr/sbin/nologin\"},{\"name\":\"statd\",\"uid\":\"106\",\"gid\":\"65534\",\"comment\":\"\",\"home\":\"/var/lib/nfs\",\"shell\":\"/bin/false\"},{\"name\":\"mail\",\"uid\":\"8\",\"gid\":\"8\",\"comment\":\"mail\",\"home\":\"/var/mail\",\"shell\":\"/usr/sbin/nologin\"},{\"name\":\"sshd\",\"uid\":\"107\",\"gid\":\"65534\",\"comment\":\"\",\"home\":\"/run/sshd\",\"shell\":\"/usr/sbin/nologin\"},{\"name\":\"news\",\"uid\":\"9\",\"gid\":\"9\",\"comment\":\"news\",\"home\":\"/var/spool/news\",\"shell\":\"/usr/sbin/nologin\"},{\"name\":\"avahi\",\"uid\":\"108\",\"gid\":\"112\",\"comment\":\"Avahi mDNS daemon,,,\",\"home\":\"/var/run/avahi-daemon\",\"shell\":\"/bin/false\"},{\"name\":\"lightdm\",\"uid\":\"109\",\"gid\":\"113\",\"comment\":\"Light Display Manager\",\"home\":\"/var/lib/lightdm\",\"shell\":\"/bin/false\"},{\"name\":\"gnats\",\"uid\":\"41\",\"gid\":\"41\",\"comment\":\"Gnats Bug-Reporting System\",\"home\":\"/var/lib/gnats\",\"shell\":\"/usr/sbin/nologin\"}]");
        assertEquals(result, referenceString);
	}
	
	@Test
	public void testSingleUserRequest() throws IOException, ServletException {
				
        when(request.getServletPath()).thenReturn("/users/3");
        when(config.getInitParameter("groupPath")).thenReturn("c:\\\\temp\\group");
        when(config.getInitParameter("passwdPath")).thenReturn("c:\\\\temp\\passwd");
     
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
         
        when(response.getWriter()).thenReturn(pw);
 
        PaS testServlet =new PaS();
        testServlet.init(config);
        testServlet.doGet(request, response);
        String result = sw.getBuffer().toString().trim();
        // System.out.println(result);
        String referenceString = new String("{\"name\":\"sys\",\"uid\":\"3\",\"gid\":\"3\",\"comment\":\"sys\",\"home\":\"/dev\",\"shell\":\"/usr/sbin/nologin\"}");
        assertEquals(result, referenceString);
	}
	
	@Test
	public void testSingleGroupRequest() throws IOException, ServletException {
				
        when(request.getServletPath()).thenReturn("/groups/3");
        when(config.getInitParameter("groupPath")).thenReturn("c:\\\\temp\\group");
        when(config.getInitParameter("passwdPath")).thenReturn("c:\\\\temp\\passwd");
     
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
         
        when(response.getWriter()).thenReturn(pw);
 
        PaS testServlet =new PaS();
        testServlet.init(config);
        testServlet.doGet(request, response);
        String result = sw.getBuffer().toString().trim();
        // System.out.println(result);
        String referenceString = new String("{\"name\":\"sys\",\"gid\":\"3\",\"members\":[]}");
        assertEquals(result, referenceString);
	}
	
	@Test
	public void testQueryeUserRequest() throws IOException, ServletException {
				
        when(request.getServletPath()).thenReturn("/users/query");
        when(request.getParameterNames() ).thenReturn( Collections.enumeration(Arrays.asList( "name", "uid","comment", "gid", "home", "shell" ) ) );
        when(request.getParameter("name")).thenReturn("sys");
        when(request.getParameter("uid")).thenReturn("3");
        when(request.getParameter("comment")).thenReturn("sys");
        when(request.getParameter("gid")).thenReturn("3");
        when(request.getParameter("home")).thenReturn("/dev");
        when(request.getParameter("shell")).thenReturn("/usr/sbin/nologin");
        when(config.getInitParameter("groupPath")).thenReturn("c:\\\\temp\\group");
        when(config.getInitParameter("passwdPath")).thenReturn("c:\\\\temp\\passwd");
     
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
         
        when(response.getWriter()).thenReturn(pw);
 
        PaS testServlet =new PaS();
        testServlet.init(config);
        testServlet.doGet(request, response);
        String result = sw.getBuffer().toString().trim();
        // System.out.println(result);
        String referenceString = new String("[{\"name\":\"sys\",\"uid\":\"3\",\"gid\":\"3\",\"comment\":\"sys\",\"home\":\"/dev\",\"shell\":\"/usr/sbin/nologin\"}]");
        assertEquals(result, referenceString);
	}
	
	@Test
	public void testGroupsUserRequest() throws IOException, ServletException {
				
        when(request.getServletPath()).thenReturn("/users/1000/groups");
        when(config.getInitParameter("groupPath")).thenReturn("c:\\\\temp\\group");
        when(config.getInitParameter("passwdPath")).thenReturn("c:\\\\temp\\passwd");
     
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
         
        when(response.getWriter()).thenReturn(pw);
 
        PaS testServlet =new PaS();
        testServlet.init(config);
        testServlet.doGet(request, response);
        String result = sw.getBuffer().toString().trim();
        // System.out.println(result);
        String referenceString = new String("[{\"name\":\"dialout\",\"gid\":\"20\",\"members\":[\"pi\"]},{\"name\":\"games\",\"gid\":\"60\",\"members\":[\"pi\"]},{\"name\":\"spi\",\"gid\":\"999\",\"members\":[\"pi\"]},{\"name\":\"netdev\",\"gid\":\"108\",\"members\":[\"pi\"]},{\"name\":\"i2c\",\"gid\":\"998\",\"members\":[\"pi\"]},{\"name\":\"gpio\",\"gid\":\"997\",\"members\":[\"pi\"]},{\"name\":\"adm\",\"gid\":\"4\",\"members\":[\"pi\"]},{\"name\":\"input\",\"gid\":\"101\",\"members\":[\"pi\"]},{\"name\":\"users\",\"gid\":\"100\",\"members\":[\"pi\",\"mail\"]},{\"name\":\"audio\",\"gid\":\"29\",\"members\":[\"pi\"]},{\"name\":\"sudo\",\"gid\":\"27\",\"members\":[\"pi\"]},{\"name\":\"plugdev\",\"gid\":\"46\",\"members\":[\"pi\"]},{\"name\":\"cdrom\",\"gid\":\"24\",\"members\":[\"pi\",\"games\"]},{\"name\":\"video\",\"gid\":\"44\",\"members\":[\"pi\",\"games\",\"mail\"]}]");
        assertEquals(result, referenceString);
	}
	
	@Test
	public void testQueryeGroupsRequest() throws IOException, ServletException {
		
        when(request.getServletPath()).thenReturn("/groups/query");
        when(request.getParameterNames() ).thenReturn( Collections.enumeration(Arrays.asList( "name", "gid", "member", "member") ) );
        when(request.getParameterValues("member") ).thenReturn( new String[] { "pi", "games" } );
        when(request.getParameter("name")).thenReturn("video");
        when(request.getParameter("gid")).thenReturn("44");
        when(config.getInitParameter("groupPath")).thenReturn("c:\\\\temp\\group");
        when(config.getInitParameter("passwdPath")).thenReturn("c:\\\\temp\\passwd");
     
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
         
        when(response.getWriter()).thenReturn(pw);
 
        PaS testServlet =new PaS();
        testServlet.init(config);
        testServlet.doGet(request, response);
        String result = sw.getBuffer().toString().trim();
        // System.out.println(result);
        String referenceString = new String("[{\"name\":\"video\",\"gid\":\"44\",\"members\":[\"pi\",\"games\",\"mail\"]}]");
        assertEquals(result, referenceString);
	}
	
	@Test
	public void testGroupRequest() throws IOException, ServletException {
				
        when(request.getServletPath()).thenReturn("/groups");
        when(config.getInitParameter("groupPath")).thenReturn("c:\\\\temp\\group");
        when(config.getInitParameter("passwdPath")).thenReturn("c:\\\\temp\\passwd");
     
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
         
        when(response.getWriter()).thenReturn(pw);
 
        PaS testServlet =new PaS();
        testServlet.init(config);
        testServlet.doGet(request, response);
        String result = sw.getBuffer().toString().trim();
        // System.out.println(result);
        String referenceString = new String("[{\"name\":\"video\",\"gid\":\"44\",\"members\":[\"pi\",\"games\",\"mail\"]},{\"name\":\"sasl\",\"gid\":\"45\",\"members\":[]},{\"name\":\"plugdev\",\"gid\":\"46\",\"members\":[\"pi\"]},{\"name\":\"ssh\",\"gid\":\"110\",\"members\":[]},{\"name\":\"bluetooth\",\"gid\":\"111\",\"members\":[]},{\"name\":\"avahi\",\"gid\":\"112\",\"members\":[]},{\"name\":\"lightdm\",\"gid\":\"113\",\"members\":[]},{\"name\":\"epmd\",\"gid\":\"114\",\"members\":[]},{\"name\":\"gpio\",\"gid\":\"997\",\"members\":[\"pi\"]},{\"name\":\"i2c\",\"gid\":\"998\",\"members\":[\"pi\"]},{\"name\":\"spi\",\"gid\":\"999\",\"members\":[\"pi\"]},{\"name\":\"staff\",\"gid\":\"50\",\"members\":[]},{\"name\":\"uucp\",\"gid\":\"10\",\"members\":[]},{\"name\":\"man\",\"gid\":\"12\",\"members\":[]},{\"name\":\"proxy\",\"gid\":\"13\",\"members\":[]},{\"name\":\"kmem\",\"gid\":\"15\",\"members\":[]},{\"name\":\"nogroup\",\"gid\":\"65534\",\"members\":[]},{\"name\":\"root\",\"gid\":\"0\",\"members\":[]},{\"name\":\"daemon\",\"gid\":\"1\",\"members\":[]},{\"name\":\"bin\",\"gid\":\"2\",\"members\":[]},{\"name\":\"sys\",\"gid\":\"3\",\"members\":[]},{\"name\":\"adm\",\"gid\":\"4\",\"members\":[\"pi\"]},{\"name\":\"tty\",\"gid\":\"5\",\"members\":[]},{\"name\":\"disk\",\"gid\":\"6\",\"members\":[]},{\"name\":\"lp\",\"gid\":\"7\",\"members\":[]},{\"name\":\"mail\",\"gid\":\"8\",\"members\":[]},{\"name\":\"news\",\"gid\":\"9\",\"members\":[]},{\"name\":\"games\",\"gid\":\"60\",\"members\":[\"pi\"]},{\"name\":\"dialout\",\"gid\":\"20\",\"members\":[\"pi\"]},{\"name\":\"fax\",\"gid\":\"21\",\"members\":[\"games\"]},{\"name\":\"voice\",\"gid\":\"22\",\"members\":[]},{\"name\":\"cdrom\",\"gid\":\"24\",\"members\":[\"pi\",\"games\"]},{\"name\":\"floppy\",\"gid\":\"25\",\"members\":[]},{\"name\":\"tape\",\"gid\":\"26\",\"members\":[]},{\"name\":\"sudo\",\"gid\":\"27\",\"members\":[\"pi\"]},{\"name\":\"audio\",\"gid\":\"29\",\"members\":[\"pi\"]},{\"name\":\"pi\",\"gid\":\"1000\",\"members\":[]},{\"name\":\"dip\",\"gid\":\"30\",\"members\":[]},{\"name\":\"www-data\",\"gid\":\"33\",\"members\":[]},{\"name\":\"backup\",\"gid\":\"34\",\"members\":[]},{\"name\":\"operator\",\"gid\":\"37\",\"members\":[]},{\"name\":\"list\",\"gid\":\"38\",\"members\":[]},{\"name\":\"irc\",\"gid\":\"39\",\"members\":[]},{\"name\":\"users\",\"gid\":\"100\",\"members\":[\"pi\",\"mail\"]},{\"name\":\"input\",\"gid\":\"101\",\"members\":[\"pi\"]},{\"name\":\"systemd-journal\",\"gid\":\"102\",\"members\":[]},{\"name\":\"systemd-timesync\",\"gid\":\"103\",\"members\":[]},{\"name\":\"systemd-network\",\"gid\":\"104\",\"members\":[]},{\"name\":\"systemd-resolve\",\"gid\":\"105\",\"members\":[]},{\"name\":\"systemd-bus-proxy\",\"gid\":\"106\",\"members\":[]},{\"name\":\"crontab\",\"gid\":\"107\",\"members\":[]},{\"name\":\"netdev\",\"gid\":\"108\",\"members\":[\"pi\"]},{\"name\":\"messagebus\",\"gid\":\"109\",\"members\":[]},{\"name\":\"src\",\"gid\":\"40\",\"members\":[]},{\"name\":\"gnats\",\"gid\":\"41\",\"members\":[]},{\"name\":\"shadow\",\"gid\":\"42\",\"members\":[]},{\"name\":\"utmp\",\"gid\":\"43\",\"members\":[]}]");
        assertEquals(result, referenceString);
	}
	
    @Mock
    HttpServletRequest request;
 
    @Mock
    HttpServletResponse response;
    
    @Mock
    ServletConfig config;
	static Properties configprop = new Properties();
	static long groupfiletimestamp;
	static long passfiletimestamp;
 
    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
    }
 


}
