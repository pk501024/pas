# PaS

**PaS Software Demo**

This project was done as a coding exercise only. This is a java servlet project that allows searching of dummy passwd and group files. You can not search the password field. It is prohibited to use this code in any way, except as a review mechanism by the assigned code reviewers. The path to the files are configured in the web.xml file located in the WEB-INF directory of the servlet installation.  One way the service can be deployed on a Tomcat server is by using a war file.

When the servlet starts the passwd and group files are indexed across all relavent fields of the query specifications. This is a one pass indexing and it is stored in memory. If the password or group files are modified this indexing is updated. Querying is supported using a stack data structure.

**Libraries needed**

The software utilizes Google's GSON for json construction. In addition Mockito is used to mock servlet request for JUNIT testing. The JUNIT libraries are also needed.

**Unit Tests**

Unit tests are configured to represent the 7 use cases from the original PaS specs document.

**API Definition**

Please see orginal PaS specs document for API definition.
