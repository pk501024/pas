
public class PassDataStore {
	
	PassDataStore(String[] strarr) throws RuntimeException{
		if((strarr!=null) && strarr.length==7) {
			Username=strarr[0];
			Password=strarr[1];
			UID=strarr[2];
			GID=strarr[3];
			Comment=strarr[4];
			HomeDir=strarr[5];
			Command=strarr[6];
		}else {
			throw new RuntimeException("PassDataStore: Malformed Input");
		}
	}
	
	String Username;
	String Password;
	String UID;
	String GID;
	String Comment;
	String HomeDir;
	String Command;
}
