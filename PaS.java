

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.ServletContext;

/**
 * Servlet implementation class PaS
 */
@WebServlet("/PaS")
public class PaS extends HttpServlet {
	private static final long serialVersionUID = 1L;
	static Properties configprop = new Properties();
	static long groupfiletimestamp;
	static long passfiletimestamp;
	static IndexedDataStore DataStore;
    /**
     * @see HttpServlet#HttpServlet()
     */
    public PaS() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see Servlet#init(ServletConfig)
	 */
	public void init(ServletConfig config) throws ServletException {
		
			configprop.setProperty("groupPath", config.getInitParameter("groupPath"));
			configprop.setProperty("passwdPath", config.getInitParameter("passwdPath"));
			/* /etc/group and /etc/passwd are default values */
			String gfilepath = configprop.getProperty("groupPath", "/etc/group");
			String pfilepath = configprop.getProperty("passwdPath", "/etc/passwd");
			DataStore = new IndexedDataStore();
			FileParse fio = new FileParse();
			fio.parsePasswordFile(pfilepath);
			fio.parseGroupFile(gfilepath);

	}

	/**
	 * @see HttpServlet#service(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		// response.getWriter().append("Served 1 at: ").append(request.getServletPath());
		FileParse fp = new FileParse();
		if(fp.CheckForPassFileUpdate()) {
			FileParse.reloadPassFile();
		}
		if(fp.CheckForGroupFileUpdate()) {
			FileParse.reloadGroupFile();
		}
		QueryData qd = new QueryData();
		qd.QueryIndexedData(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
